﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApiTest.Services
{
    public class Core
    {
        public static async Task<Weather> GetWeather(string city, string countryIso, string units)
        {
            //Sign up for a free API key at http://openweathermap.org/appid
            var key = "a702eeaae7db5f7c0233a616acca2422";
            var queryString =
                $"http://api.openweathermap.org/data/2.5/weather?q={city},{countryIso}&appid={key}&units={units}";

            dynamic results = await DataService.getDataFromService(queryString).ConfigureAwait(false);

            if (results["weather"] != null)
            {
                Weather weather = new Weather();
                weather.Title = (string)results["name"];
                weather.Temperature = (string)results["main"]["temp"];
                weather.Wind = (string)results["wind"]["speed"];
                weather.Humidity = (string)results["main"]["humidity"];
                weather.Visibility = (string)results["weather"][0]["main"];

                DateTime time = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
                DateTime sunrise = time.AddSeconds((double)results["sys"]["sunrise"]);
                DateTime sunset = time.AddSeconds((double)results["sys"]["sunset"]);
                weather.Sunrise = sunrise.ToString();
                weather.Sunset = sunset.ToString();
                return weather;
            }
            else
            {
                return null;
            }
        }
    }
}
