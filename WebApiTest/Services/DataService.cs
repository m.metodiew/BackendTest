﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace WebApiTest.Services
{
    public class DataService
    {
        public static async Task<dynamic> getDataFromService(string queryString)
        {
            HttpClient client = new HttpClient();
            var response = await client.GetAsync(queryString);

            if (response == null)
                throw new InvalidOperationException("Invalid weather service response");

            if (response.StatusCode != HttpStatusCode.OK)
                throw new InvalidOperationException("Invalid weather service response code");

            string json = await response.Content.ReadAsStringAsync();
            var data = JsonConvert.DeserializeObject(json);

            return data;
        }
    }
}
