﻿using System.Reflection;
using Microsoft.Owin;
using Owin;
using IdentityServer3.AccessTokenValidation;
using System.Web.Http;
using ApplicationInsights.OwinExtensions;
using Autofac;
using Autofac.Integration.WebApi;
using Microsoft.Azure;
using WebApiTest.Controllers;

[assembly: OwinStartup(typeof(WebApiTest.Startup))]

namespace WebApiTest
{
    public class Startup
    {
        private readonly ITelemetryLogger _log = new TelemetryLogger();

        public void Configuration(IAppBuilder app)
        {
            _log.Information("WebApiTest Startup");

            var authority = CloudConfigurationManager.GetSetting("Authority");

            // AppInsights not supported out of the box for OWIN
            //https://visualstudio.uservoice.com/forums/357324-application-insights/suggestions/10056219-support-owin-middleware-in-application-insights
            app.UseApplicationInsights();

            app.UseIdentityServerBearerTokenAuthentication(new IdentityServerBearerTokenAuthenticationOptions
            {
                Authority = authority,
                RequiredScopes = new[] { "weather" }
            });

            // configure web api
            var config = new HttpConfiguration();

            //autofac
            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterType<TelemetryLogger>().As<ITelemetryLogger>()
                .InstancePerRequest();
            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            //routes
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // require authentication for all controllers
            config.Filters.Add(new AuthorizeAttribute());

            SwaggerConfig.Register(config, authority);

            app.UseWebApi(config);

        }
    }
}
