﻿using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using WebApiTest.Services;

namespace WebApiTest.Controllers
{
    /// <summary>
    /// Weather API
    /// </summary>
    public class WeatherController : ApiController
    {
        private readonly ITelemetryLogger _log;

        public WeatherController(ITelemetryLogger log)
        {
            _log = log;
        }

        /// <summary>
        /// Gets the weather for a given city
        /// </summary>
        /// <param name="cityName">City name</param>
        /// <param name="countryIso">Country ISO code - bg, us, ...</param>
        /// <param name="units">Result data units - metric, imperial</param>
        /// <returns>The weather</returns>
        /// <response code="200">Returns the weather</response>
        /// <response code="400">Invalid input parameters</response>
        /// <response code="404">City not found by the given name and country ISO code</response>
        /// <response code="500">Fatal error</response>
        [ResponseType(typeof(Weather))]
        public async Task<IHttpActionResult> Get(string cityName, string countryIso, string units)
        {
            _log.Information($"GetWeather({cityName},{countryIso},{units})");

            if (string.IsNullOrEmpty(cityName))
                return BadRequest();

            if (string.IsNullOrEmpty(countryIso))
                return BadRequest();

            if (string.IsNullOrEmpty(units))
                return BadRequest();

            var weather = await Core.GetWeather(cityName, countryIso, units);

            if (weather == null)
                return NotFound();

            _log.Information($"GetWeather() => Ok({weather.Temperature})");

            return Ok(weather);
        }
    }
}
