﻿using System.Diagnostics;
using Microsoft.ApplicationInsights;

namespace WebApiTest.Controllers
{
    public interface ITelemetryLogger
    {
        void Information(string str);
    }

    public class TelemetryLogger : ITelemetryLogger
    {
        private readonly TelemetryClient _telemetry = new TelemetryClient();

        public void Information(string str)
        {
            Trace.TraceInformation(str);

            _telemetry.TrackTrace(str);
        }
    }
}