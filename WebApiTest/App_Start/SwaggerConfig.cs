using System.Web.Http;
using Swashbuckle.Application;
using Swashbuckle.Swagger;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Description;

namespace WebApiTest
{
    public class AssignOAuth2SecurityRequirements : IOperationFilter
    {
        public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
        {
            if (operation.security == null)
                operation.security = new List<IDictionary<string, IEnumerable<string>>>();

            var oAuthRequirements = new Dictionary<string, IEnumerable<string>>
            {
                { "oauth2", Enumerable.Empty<string>() }
            };

            operation.security.Add(oAuthRequirements);
        }
    }

    public class SwaggerConfig
    {
        public static void Register(HttpConfiguration config, string authority)
        {

            var xmlDocPath = $@"{System.AppDomain.CurrentDomain.BaseDirectory}\bin\SwaggerDocs.XML";

            //swagger
            config.EnableSwagger(c =>
            {
                c.SingleApiVersion("v1", "Weather API");


                c.OAuth2("oauth2")
                    .Description("OAuth2 Implicit Grant")
                    .Flow("implicit")
                    .AuthorizationUrl($"{authority}/connect/authorize")
                    .Scopes(scopes =>
                    {
                        scopes.Add("weather", "Access weather data");
                    });
                c.IncludeXmlComments(xmlDocPath);
                c.OperationFilter<AssignOAuth2SecurityRequirements>();

            })
                .EnableSwaggerUi(c =>
                {
                    c.EnableOAuth2Support("weather.implicit", "notused", "Swagger UI");
                });

        }
    }
}
